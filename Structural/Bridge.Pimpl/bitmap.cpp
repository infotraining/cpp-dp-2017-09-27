#include "bitmap.hpp"
#include <algorithm>
#include <array>

using namespace std;

struct Bitmap::BitmapImpl
{
    std::vector<char> image_;
};

Bitmap::~Bitmap() = default;

Bitmap::Bitmap(size_t size, char fill_char) : impl_{make_unique<BitmapImpl>()}
{
    impl_->image_.resize(size);
    fill_n(impl_->image_.begin(), size, fill_char);
}

void Bitmap::draw()
{
    cout << "Image: ";
    for (size_t i = 0; i < impl_->image_.size(); ++i)
        cout << impl_->image_[i];
    cout << endl;
}
