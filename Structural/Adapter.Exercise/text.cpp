#include "text.hpp"
#include "shape_factories.hpp"

namespace
{
    using namespace Drawing;
    using namespace std;

    bool is_registered =
            SingletonShapeFactory::instance()
                .register_creator(Text::id, &make_unique<Text>);
}
