#include "starbugs_coffee.hpp"
#include <cassert>
#include <memory>

void client(std::unique_ptr<Coffee> coffee)
{
    std::cout << "Description: " << coffee->get_description() << "; Price: " << coffee->get_total_price() << std::endl;
    coffee->prepare();
}

class CoffeeBuilder
{
    CoffeePtr coffee_;

public:
    template <typename Base>
    CoffeeBuilder& create_base()
    {
        coffee_ = std::make_unique<Base>();

        return *this;
    }

    template <typename Condiment>
    CoffeeBuilder& add()
    {
        assert(coffee_ != nullptr);
        coffee_ = std::make_unique<Condiment>(std::move(coffee_));

        return *this;
    }

    CoffeePtr get_coffee()
    {
        return std::move(coffee_);
    }
};

int main()
{
    std::unique_ptr<Coffee> cf = std::make_unique<Whipped>(
        std::make_unique<Whisky>(
            std::make_unique<Whisky>(
                std::make_unique<ExtraEspresso>(
                    std::make_unique<Espresso>()))));
    client(std::move(cf));

    CoffeeBuilder cb;
    cb.create_base<Espresso>().add<Whisky>().add<Whipped>();
    client(cb.get_coffee());
}
