#include <cassert>
#include <iostream>
#include <memory>
#include <typeinfo>
#include <vector>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;

    std::unique_ptr<Engine> clone() const
    {
        auto cloned_engine = do_clone();
        assert(typeid(*cloned_engine) == typeid(*this));

        return cloned_engine;
    }

    virtual ~Engine() = default;
protected:
    virtual std::unique_ptr<Engine> do_clone() const = 0;
};


// CRTP
template <typename EngineType, typename EngineBase = Engine>
class CloneableEngine : public EngineBase
{
protected:
    std::unique_ptr<Engine> do_clone() const
    {
        return make_unique<EngineType>(static_cast<const EngineType&>(*this));
    }
};

class Diesel : public CloneableEngine<Diesel>
{
public:
    virtual void start() override
    {
        cout << "Diesel starts\n";
    }

    virtual void stop() override
    {
        cout << "Diesel stops\n";
    }
};

class TDI : public CloneableEngine<TDI, Diesel>
{
public:
    virtual void start() override
    {
        cout << "TDI starts\n";
    }

    virtual void stop() override
    {
        cout << "TDI stops\n";
    }
};

class Hybrid : public CloneableEngine<Hybrid>
{
public:
    virtual void start() override
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop() override
    {
        cout << "Hybrid stops\n";
    }
};

class Car
{
    std::unique_ptr<Engine> engine_;

public:
    Car(std::unique_ptr<Engine> engine) : engine_{ move(engine) }
    {
        assert(engine_ != nullptr);
    }

    Car(const Car& source) : engine_{source.engine_->clone()}
    {
    }

    Car& operator=(const Car& source)
    {
        Car temp(source);
        swap(temp);

        return *this;
    }

    void swap(Car& other)
    {
        std::swap(engine_, other.engine_);
    }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

namespace LegacyCode
{
    Engine* create_engine()
    {
        return new TDI();
    }

    void use(Engine* e)
    {
        e->start();
        e->stop();

        //delete e;
    }
}

namespace ModernCpp
{
    std::unique_ptr<Engine> create_engine()
    {
        return make_unique<TDI>();
    }

    unique_ptr<Engine> e = create_engine();
    unique_ptr<Engine> ce = move(e);

    void use(Engine* e)
    {
        e->start();
        e->stop();
    }

    vector<unique_ptr<Engine>> engines;

    void use(std::unique_ptr<Engine> e)
    {
        e->start();
        e->stop();

        engines.push_back(move(e));
    }

    void foo()
    {
        use(create_engine());

        engines.clear();
    }
}

int main()
{
    Car c1{ make_unique<TDI>() };
    c1.drive(100);    

    cout << "\n";

    Car c2 = c1;
    c2.drive(200);
}
