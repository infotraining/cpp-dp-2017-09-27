#include <memory>
#include <vector>
#include <functional>
#include <unordered_map>
#include <typeindex>

#include "employee.hpp"
#include "hrinfo.hpp"

//HRInfo* gen_info(const Employee& e)
//{
//    if (const Salary* s = dynamic_cast<const Salary*>(&e))
//        return new StdInfo(s);
//    if (const Hourly* h = dynamic_cast<const Hourly*>(&e))
//        return new StdInfo(h);
//    if (const Temp* t = dynamic_cast<const Temp*>(&e))
//        return new TempInfo(t);

//    throw std::runtime_error("Wrong employee type");
//}

using ReportCreator = std::function<std::unique_ptr<HRInfo>(const Employee*)>;

template <typename T>
std::type_index make_type_index(T& item)
{
    return std::type_index{typeid(item)};
}

template <typename T>
std::type_index make_type_index()
{
    return std::type_index{typeid(T)};
}

class ReportFactory
{
    std::unordered_map<std::type_index, ReportCreator> creators_;
public:
    bool register_creator(std::type_index id, ReportCreator creator)
    {
        return creators_.insert(std::make_pair(id, creator)).second;
    }

    std::unique_ptr<HRInfo> create_info(const Employee& emp)
    {
        return creators_.at(make_type_index(emp))(&emp);
    }
};

void create_report(const std::vector<std::unique_ptr<Employee>>& emps, ReportFactory& report_factory)
{
    for (const auto& emp : emps)
    {
        auto hri = report_factory.create_info(*emp);
        hri->info();
        std::cout << std::endl;
    } // wyciek pamięci
}

int main()
{
    using namespace std;

    vector<std::unique_ptr<Employee>> emps;
    emps.push_back(make_unique<Salary>("Jan Kowalski"));
    emps.push_back(make_unique<Hourly>("Adam Nowak"));
    emps.push_back(make_unique<Temp>("Anna Nowakowska"));

    cout << "HR Report:\n---------------\n";
    // generowanie obiektów typu HRInfo
    ReportFactory rf;

    rf.register_creator(make_type_index<Salary>(), [](const auto* e) { return make_unique<StdInfo>(e); });
    rf.register_creator(make_type_index<Hourly>(), [](const auto* e) { return make_unique<StdInfo>(e); });
    rf.register_creator(make_type_index<Temp>(), [](const auto* e) { return make_unique<TempInfo>(e); });

    create_report(emps, rf);
}
