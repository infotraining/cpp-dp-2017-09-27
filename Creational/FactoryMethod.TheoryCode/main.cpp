#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <functional>

#include "factory.hpp"

using namespace std;

using LoggerCreator = function<unique_ptr<Logger>()>;

class Service
{
    LoggerCreator make_logger_;

public:
    Service(const LoggerCreator& creator) : make_logger_(creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void bad_use()
    {
        ConsoleLogger logger;
        logger.log("start");
        run();
        logger.log("end");
    }

    void use()
    {
        unique_ptr<Logger> logger = make_logger_();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }
protected:
    virtual void run() {}
};

using LoggerFactory = std::unordered_map<std::string, LoggerCreator> ;

void f1(int a)
{
    cout << "f1(" << a << ")" << endl;
}

struct F1
{
    void operator()(int a)
    {
        cout << "F1::operator()(" << a << ")" << endl;
    }
};

void function_intro()
{
    // 1-st callable
    void(*ptr)(int) = &f1;
    ptr(10);

    // 2-nd callable
    F1 func;
    func(20);

    // 3-rd callable
    auto l = [](int a) { cout << "lambda(" << a << ")" << endl; };
    l(30);

    function<void(int)> f;
    f = &f1;
    f(10);

    f = func;
    f(20);

    f = l;
    f(30);
}

int main()
{
    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger", &make_unique<ConsoleLogger>));
    logger_factory.insert(make_pair("FileLogger", [] { return make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", &make_unique<DbLogger>));

    Service srv(logger_factory.at("DbLogger"));
    srv.use();
}
