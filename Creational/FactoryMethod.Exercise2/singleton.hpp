#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <iostream>

template <typename T>
class SingletonHolder
{
private:
    SingletonHolder() = default;
    ~SingletonHolder() = default;

public:
    SingletonHolder(const SingletonHolder&) = delete;
    SingletonHolder& operator=(const SingletonHolder&) = delete;

    static T& instance()
    {
        static T unique_instance;

        return unique_instance;
    }
};

namespace BadImplementation
{
    template <typename T>
    class SingletonHolder
    {
    private:
        SingletonHolder() = default;
        ~SingletonHolder() = default;
        static T* instance_;

    public:
        SingletonHolder(const SingletonHolder&) = delete;
        SingletonHolder& operator=(const SingletonHolder&) = delete;

        static T& instance()
        {
            if (instance_ == nullptr)
                instance_ = new T();

            return *instance_;
        }
    };

    template <typename T>
    T* SingletonHolder<T>::instance_ = nullptr;
}

#endif // SINGLETON_HPP
