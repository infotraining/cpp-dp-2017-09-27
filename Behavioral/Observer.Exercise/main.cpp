#include "stock.hpp"

using namespace std;

void log(string msg)
{
    std::cout << "LOG: " << msg << std::endl;
}

int main()
{
    Stock misys("Misys", 340.0);
    Stock ibm("IBM", 245.0);
    Stock tpsa("TPSA", 95.0);

    Investor kulczyk_jr{"Kulczyk Jr"};
    Investor solorz{"Solorz"};

    // rejestracja inwestorow zainteresowanych powiadomieniami o zmianach kursu spolek
    misys.connect([&](string s, double p) { kulczyk_jr.update_portfolio(s, p);});
    ibm.connect([&](string s, double p) { kulczyk_jr.update_portfolio(s, p);});
    tpsa.connect([&](string s, double p) { solorz.update_portfolio(s, p);});
    auto conn2log = ibm.connect([](string s, double p) { log(s + " - " + to_string(p) + "$");});

    // zmian kursow
    misys.set_price(360.0);
    ibm.set_price(210.0);
    tpsa.set_price(45.0);

    cout << "\n\n";

    conn2log.disconnect();

    misys.set_price(380.0);
    ibm.set_price(230.0);
    tpsa.set_price(15.0);
}
